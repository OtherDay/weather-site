<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', function()
{
	if (Cache:: has('wellington_weather')) {

		$wellington_weather = Cache::get('wellington_weather');

	} else {

		$wellington_weather = Weather::getWeather('Wellington, New Zealand');

		Cache::add('wellington_weather', $wellington_weather, 30);

	}	

	return View::make('weather')->withWeather($wellington_weather);


});
