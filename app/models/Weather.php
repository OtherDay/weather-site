<?php


class Weather {

    public static function getWeather($location) {

        $yql = 'SELECT * FROM weather.bylocation WHERE location="';
        $yql .= $location . '" and unit="c"';

        $yqlResult = self::getYQLResult($yql);

        $location = $yqlResult->weather->rss->channel->location;
        $conditions = $yqlResult->weather->rss->channel->item->condition;

        $result = new stdClass();
        $result->cityname = $location->city;
        $result->countryname = $location->country;
        $result->temperature = $conditions->temp;
        $result->conditions = $conditions->text;

        return $result;
    }

    private static function getYQLResult($yql) {
        $url = "https://query.yahooapis.com/v1/public/yql?q=";
        $url .= urlencode($yql);
        $url .= "&format=json&env=" . urlencode ("store://datatables.org/alltableswithkeys") . "&callback=";

        $json = self::getHTTPResponse($url);

        $result = json_decode($json);

        return $result->query->results;
    }

    private static function getHTTPResponse($url) {
        $curlHandler = curl_init();

        $options = array (
            CURLOPT_RETURNTRANSFER  => true,    // return web page
            CURLOPT_HEADER          => false,   // don't return headers
            CURLOPT_FOLLOWLOCATION  => true,    // follow redirects
            CURLOPT_ENCODING        => "",      // handle compressed
            CURLOPT_AUTOREFERER     => true,    // set referer on redirect
            CURLOPT_CONNECTTIMEOUT  => 120,     // timeout on connect
            CURLOPT_TIMEOUT         => 120,     // timeout on response
            CURLOPT_MAXREDIRS       => 10,      // stop after 10 redirects

            // YOOBEE PROXY STUFF

            CURLOPT_PROXY           => 'proxy',
            CURLOPT_PROXYPORT       => '3128',
            CURLOPT_PROXYUSERPWD    =>  'jonas.ermen:sentinel'
         );

        //  tell curl to return the response as a string
        curl_setopt_array($curlHandler, $options);

        curl_setopt($curlHandler, CURLOPT_URL, $url);

        return curl_exec($curlHandler);
    }

}